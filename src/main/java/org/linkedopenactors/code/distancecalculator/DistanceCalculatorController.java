package org.linkedopenactors.code.distancecalculator;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DistanceCalculatorController {

	@GetMapping(path = "/algorithms/DistanceCalculator", produces = { "text/html" })
	public String getDistanceCalculator(Model model) {
//		DistanceCalculator distanceCalculator = new DistanceCalculator();
//		model.addAttribute("distanceCalculator", distanceCalculator);
		return "DistanceCalculator";
	}
}
