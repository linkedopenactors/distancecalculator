package org.linkedopenactors.code.distancecalculator;

import java.util.Comparator;

import de.naturzukunft.rdf4j.ommapper.BaseObject;
import de.naturzukunft.rdf4j.ommapper.Iri;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@Iri("http://purl.org/spar/fabio/Algorithm" )
public class DistanceCalculator extends BaseObject implements Comparator<GeoCoordinates> {

	@Iri("http://schema.org/name" )
	private String name;
	
	@Iri("http://schema.org/description" )
	private String description;
	
	@Iri("http://schema.org/mainEntityOfPage" )
	private String howTo;
	
	public DistanceCalculator() {
		this.name = this.getClass().getSimpleName();
		this.description = "Calculating the distance between to geo locations.";
		this.howTo = "http://localhost:8080/algorithms/DistanceCalculator";
	}
	
	@Override
	public int compare(@NonNull GeoCoordinates geoCoordinatesA, @NonNull GeoCoordinates geoCoordinatesB) {
		Double distance = distanceInKm(geoCoordinatesA.getLatitude(), geoCoordinatesA.getLongitude(), geoCoordinatesB.getLatitude(), geoCoordinatesB.getLongitude());
		return distance.intValue();
	}

	/**
	 * "Stolen" from here: https://www.daniel-braun.com/technik/distanz-zwischen-zwei-gps-koordinaten-in-java-berchenen
	 * @return distance in kilometers
	 */
	private double distanceInKm(double lat1, double lon1, double lat2, double lon2) {
	    int radius = 6371;

	    double lat = Math.toRadians(lat2 - lat1);
	    double lon = Math.toRadians(lon2- lon1);

	    double a = Math.sin(lat / 2) * Math.sin(lat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(lon / 2) * Math.sin(lon / 2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	    double d = radius * c;

	    return Math.abs(d);
	}
}
